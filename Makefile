classes = $(shell find -L default/filters/available/documentclass/. -maxdepth 1 -type d | sed 's/.*\///;/^\.$$/d')
targets = $(classes:=.md) $(classes:=.tex) $(classes:=.pdf) $(classes:=.png) $(classes:=.zip) $(classes:=.readme.md) $(classes:=.readme.html) index.md index.html
temp := $(shell mktemp -d)
timestamp=$(shell date -u -Isecond)

all: $(targets)

submodules:
	git submodule init
	git submodule update
	$(MAKE) -C default submodules

index.md: Makefile
	echo '---' > $@
	printf "title: Overview of Pandoc templates for scientific papers\nauthor: Desmond Kabus\ndate: $(timestamp)\n" >> $@
	echo '---' >> $@
	echo >> $@
	cat README.md >> $@
	echo $(classes) | xargs -n1 | while read class; do \
		echo; \
		echo "# Template \`$$class\`:" ; \
		echo; \
		echo "- [README information]($$class.readme.html)" ; \
		echo "- [ZIP file of the project folder]($$class.zip)" ; \
		echo "- [Markdown code]($$class.md)" ; \
		echo "- [generated LaTeX code]($$class.tex)" ; \
		echo "- [generated PDF document]($$class.pdf)" ; \
		echo "- [PNG image of the first page]($$class.png)" ; \
		echo; \
		echo "![First page of the \`$$class\` template]($$class.png)" ; \
	done >> $@

%.html: %.md
	pandoc -s -o $@ $<

$(classes:=.readme.md): %.readme.md: default/filters/available/documentclass/%/README.md
	cp $< $@

$(classes:=.zip): %.zip: %.tex
$(classes:=.md): %.md: %.tex
$(classes:=.pdf): %.pdf: %.tex
$(classes:=.tex): %.tex: default/filters/available/documentclass/% default/main.md
	$(RM) -r $(temp)
	mkdir -p $(temp)
	cp -r default $(temp)/default
	$(RM) -r $(temp)/default/.git $(temp)/default/.gitmodules $(temp)/default/filters/available
	cd $(temp)/default \
		&& git init --initial-branch=main \
		&& git config user.email desmond.kabus@protonmail.com \
		&& git config user.name 'Desmond Kabus' \
		&& git submodule add https://gitlab.com/dkabus/pandoc-filters-dkabus.git filters/available \
		&& make submodules \
		&& git commit -m 'Add submodule' \
		&& git add . \
		&& git commit -m 'Initial document structure'
	$(RM) $(temp)/default/filters/0-documentclass-default.lua
	@cd $(temp) \
		&& find -L $< -maxdepth 1 -type f -iname '*.lua' -or -iname '*.sed' \
		| sed 's|\(default/filters\)/\(.*\)/\(.*\)|ln -s "\2/\3" "\1/0-\3"|' \
		| tee | sh -
	@cd $(temp) \
		&& test -d $</latex \
		&& find -L $</latex -maxdepth 1 -type f \
		| sed 's|\(default\)/\(.*\)/\(.*\)|ln -s "\2/\3" "\1/\3"|' \
		| tee | sh - \
		|| :
	cd $(temp) \
		&& test -f "$(PWD)/$(@:tex=diff)" \
		&& git apply --directory=default "$(PWD)/$(@:tex=diff)" \
		|| :
	cd $(temp)/default \
		&& git add . \
		&& git commit -m 'Switch to template "$(@:.tex=)"' \
		&& git config --unset user.email \
		&& git config --unset user.name
	cd $(temp)/default \
		&& zip --symlinks -r "$(PWD)/$(@:tex=zip)" .
	$(MAKE) -C $(temp)/default main.tex main.pdf
	mv $(temp)/default/main.tex $@
	mv $(temp)/default/main.pdf $(@:tex=pdf)
	mv $(temp)/default/main.md $(@:tex=md)
	$(RM) -r $(temp)

%.png: %.pdf
	pdftoppm $(shell test "$(@:.png=)" = "elsevier" && echo '-f 2' || echo '') -png -singlefile $< $(@:.png=)

clean:
	$(RM) $(targets)

.NOTPARALLEL:
