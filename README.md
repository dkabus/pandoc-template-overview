# Overview of Pandoc templates for scientific papers

This repository is a showcase of these Pandoc templates and filters:

- <https://dkabus.gitlab.io/pandoc-template-scientific-paper/>
- <https://gitlab.com/dkabus/pandoc-template-scientific-paper/>
- <https://gitlab.com/dkabus/pandoc-filters-dkabus>

Check out these repositories to see how to use the templates.

You can find the showcase of the templates here:

- <https://dkabus.gitlab.io/pandoc-template-overview/>
- <https://gitlab.com/dkabus/pandoc-template-overview/>
